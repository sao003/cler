package com.wecredible.cler;

import android.content.res.Configuration;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.util.Formatter;


public class parentLoansActivity extends ActionBarActivity {
    private int loanStatus;
    private int requestedAmount;
    private int currLoan;

    private ParseQuery<ParseObject> query = ParseQuery.getQuery("target");

    private EditText message;
    private CheckBox checkBoxYes;
    private CheckBox checkBoxNo;
    private Button submitButton;
    private TextView requestStatus;
    private EditText interest;
    private EditText datePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_loans);

        query.getInBackground(CurrSession.getUserID(), new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    loanStatus = object.getInt("loanStatus");
                    requestedAmount = object.getInt("requestedLoanAmount");
                    currLoan = object.getInt("loans");
                    updateStatus(loanStatus, requestedAmount);
                }
                else {
                    Log.d("loanStatus", "Fetching failed");
                }
            }
        });

        requestStatus = (TextView) findViewById(R.id.parent_request_amount_text_view);
        checkBoxYes = (CheckBox) findViewById(R.id.parent_checkbox_yes);
        checkBoxNo = (CheckBox) findViewById(R.id.parent_checkbox_no);
        datePicker = (EditText) findViewById(R.id.parent_due_edit_text);
        message = (EditText) findViewById(R.id.parent_msg_edit_text);
        submitButton = (Button) findViewById(R.id.parent_submit_button);
        interest = (EditText) findViewById(R.id.parent_interest_rate_edit_text);

        checkBoxYes.setChecked(true);

        checkBoxYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkBoxNo.setChecked(false);
                }
                else {
                    checkBoxNo.setChecked(true);
                }
            }
        });
        checkBoxNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkBoxYes.setChecked(false);
                }
                else {
                    checkBoxYes.setChecked(true);

                }
            }
        });

        interest.setRawInputType(Configuration.KEYBOARD_12KEY);

        interest.addTextChangedListener(new TextWatcher() {
            private String current = "";

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    interest.removeTextChangedListener(this);
                    String cleanString = s.toString().replaceAll("[^\\d]", "");
                    try {
                        double parsed = Double.parseDouble(cleanString);
                        String formatted = NumberFormat.getNumberInstance()
                                .format((parsed / 100));
                        current = formatted;
                        interest.setText(formatted +"%");
                        interest.setSelection(formatted.length());
                        interest.addTextChangedListener(this);
                    } catch (NumberFormatException e) {
                        // do nothing
                    }
                }
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                query.getInBackground(CurrSession.getUserID(), new GetCallback<ParseObject>() {
                    public void done(ParseObject object, ParseException e) {
                        if (e == null) {
                            object.put("loans", currLoan+requestedAmount);
                            object.put("interestRate", Integer.parseInt(interest.getText().toString().replaceAll("[^\\d]","")));
                            object.put("message", message.getText().toString());


                            if (checkBoxYes.isChecked()) {
                                object.put("isInDebt",true);
                                object.put("loanStatus",2);
                            }
                            else {
                                object.put("isInDebt",false);
                                object.put("loanStatus",3);
                            }

                            object.saveInBackground();
                        }
                    }
                });

                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_parent_loans, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateStatus(int status, int amount) {
        if(status == 0) {
            message.setEnabled(false);
            interest.setEnabled(false);
            checkBoxYes.setEnabled(false);
            checkBoxNo.setEnabled(false);
            datePicker.setEnabled(false);
            submitButton.setText("Reset");
            requestStatus.setText("Great! You do not have any request.");
        }

        else if (status == 1) {
            requestStatus.setText("Requested: Loan amount of " +
                    currencyFormat(amount) +
                    " was requested.");
            requestStatus.setTextColor(Color.BLACK);
        }

        else if (status == 2) {
            message.setEnabled(false);
            interest.setEnabled(false);
            checkBoxYes.setEnabled(false);
            checkBoxNo.setEnabled(false);
            datePicker.setEnabled(false);
            submitButton.setText("Reset");
            requestStatus.setText("Accepted: You have accepted amount of " +
                    currencyFormat(amount) +
                    ".");
            requestStatus.setTextColor(Color.GREEN);
        }

        else {
            message.setEnabled(false);
            interest.setEnabled(false);
            checkBoxYes.setEnabled(false);
            checkBoxNo.setEnabled(false);
            datePicker.setEnabled(false);
            submitButton.setText("Reset");
            requestStatus.setText("Denied: You have denied amount of " +
                    currencyFormat(amount) +
                    ".");
            requestStatus.setTextColor(Color.RED);
        }
    }

    private String currencyFormat(int money) {
        double parsed = Double.parseDouble(String.valueOf(money));
        String s = NumberFormat.getCurrencyInstance()
                .format((parsed / 100));
        return s;
    }

    private String interestFormatter(String num) {
        String cleanString = num.replaceAll("[%,.]", "");
        double parsed = Double.parseDouble(cleanString);
        String s = NumberFormat.getNumberInstance()
                .format((parsed / 100));
        s += "%";
        return s;
    }

}
