package com.wecredible.cler;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.NumberFormat;

/**
 * Created by pablo on 4/4/2015.
 */
public class childLoansActivity extends ActionBarActivity {
    private int loanStatus;
    private int requestedAmount;
    private boolean isInDebt;

    private ParseQuery<ParseObject> query = ParseQuery.getQuery("target");

    private EditText requestedAmnt;
    private CheckBox checkBox;
    private Button requestButton;
    private Button okayButton;
    private Button okayButton2;
    private TextView requestStatus;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_loans);

        query.getInBackground(CurrSession.getUserID(), new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    loanStatus = object.getInt("loanStatus");
                    requestedAmount = object.getInt("requestedLoanAmount");
                    isInDebt = object.getBoolean("isInDebt");
                    isInDebt = true;
                    updateStatus(loanStatus, requestedAmount, isInDebt);
                }
                else {
                    Log.d("loanStatus", "Fetching failed");
                }
            }
        });

        requestedAmnt = (EditText) findViewById(R.id.requestLoan_view);

        checkBox = (CheckBox) findViewById(R.id.loan_disclaimer_checkbox);
        requestButton = (Button) findViewById(R.id.requestLoan_button);
        requestStatus = (TextView) findViewById(R.id.loan_request_status);
        okayButton = (Button) findViewById(R.id.child_loan_okay_button);
        okayButton2 = (Button) findViewById(R.id.child_loan_okay_button2);

        requestStatus.setVisibility(View.VISIBLE);

        requestButton.setEnabled(false);

        requestedAmnt.addTextChangedListener(new TextWatcher() {

            private String current = "";

            @Override
            public void afterTextChanged(Editable s) { }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    requestedAmnt.removeTextChangedListener(this);
                    String cleanString = s.toString().replaceAll("[^\\d]", "");
                    try {
                        double parsed = Double.parseDouble(cleanString);
                        String formatted = NumberFormat.getCurrencyInstance()
                                .format((parsed / 100));
                        current = formatted;
                        requestedAmnt.setText(formatted);
                        requestedAmnt.setSelection(formatted.length());
                        requestedAmnt.addTextChangedListener(this);
                    } catch (NumberFormatException e) {
                        // do nothing
                    }
                }
            }
        });

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //if the enter key was pressed, then hide the keyboard and do whatever needs doing.
                InputMethodManager imm = (InputMethodManager) childLoansActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(requestedAmnt.getApplicationWindowToken(), 0);

                if (checkBox.isChecked() && !requestedAmnt.getText().toString().equals("$")) {
                    requestButton.setEnabled(true);
                }

                else {
                    requestButton.setEnabled(false);
                }
            }
        });

        requestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loanStatus == 0) {
                    requestButton.setEnabled(false);

                    requestStatus.setText("Pending: Your loan amount of " +
                            requestedAmnt.getText().toString() +
                            " was requested.");
                    requestStatus.setTextColor(Color.BLACK);


                    query.getInBackground(CurrSession.getUserID(), new GetCallback<ParseObject>() {
                        public void done(ParseObject object, ParseException e) {
                            if (e == null) {
                                object.put("loanStatus", 1);
                                String clearNumber = requestedAmnt.getText().toString().replaceAll("[^\\d]", "");
                                int parsed = Integer.parseInt(clearNumber);
                                object.put("requestedLoanAmount", parsed);
                                object.put("isInDebt", true);
                                object.saveInBackground();
                            } else {
                                Log.d("loan", "WTF IS GOING ON?");
                            }
                        }
                    });
                }
            }
        });

        requestedAmnt.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                //if the enter key was pressed, then hide the keyboard and do whatever needs doing.
                InputMethodManager imm = (InputMethodManager) childLoansActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(requestedAmnt.getApplicationWindowToken(), 0);

                return true;
            }

            return false;
            }
        });

        okayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInDebt) {
                    Intent intent = new Intent(childLoansActivity.this, childChoresActivity.class);
                    startActivity(intent);
                }
                else {
                    //finish();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_child_loans, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateStatus(int status, int amount, boolean isInDebt) {
        if(status == 0) {
            requestStatus.setText("Great! You do not have any request.");
        }

        else if (status == 1) {
            requestedAmnt.setEnabled(false);
            checkBox.setEnabled(false);
            requestStatus.setText("Pending: Your loan amount of " +
                    currencyFormat(amount) +
                    " was requested.");
            requestStatus.setTextColor(Color.BLACK);
        }

        else if (status == 2) {
            requestedAmnt.setEnabled(false);
            checkBox.setEnabled(false);
            requestStatus.setText("Accepted: Your loan amount of " +
                    currencyFormat(amount) +
                    " was accepted.");
            requestStatus.setTextColor(Color.GREEN);
        }

        else {
            requestedAmnt.setEnabled(false);
            checkBox.setEnabled(false);
            requestStatus.setText("Denied: Your loan amount of " +
                    currencyFormat(amount) +
                    " was denied.");
            requestStatus.setTextColor(Color.RED);
        }

        if (isInDebt) {
            okayButton.setText("Pay Debt");
            okayButton2.setVisibility(View.VISIBLE);
            okayButton2.setText("Payment Options");
        }
        else {
            okayButton2.setVisibility(View.INVISIBLE);
        }
    }

    private String currencyFormat(int money) {
        double parsed = Double.parseDouble(String.valueOf(money));
        String s = NumberFormat.getCurrencyInstance()
                .format((parsed / 100));
        return s;
    }
}
