package com.wecredible.cler;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MotionEvent;


public class Splash extends ActionBarActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splash);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_UP:
                Intent mainIntent = new Intent(Splash.this,whoIsItActivity.class);
                Splash.this.startActivity(mainIntent);
                Splash.this.finish();
                break;
        }
        return true;
    }
}
