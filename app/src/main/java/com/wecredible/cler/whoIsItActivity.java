package com.wecredible.cler;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.parse.Parse;


public class whoIsItActivity extends ActionBarActivity {
    private static boolean init = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        View view = findViewById(android.R.id.content);
        Animation mLoadAnimation = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in);
        mLoadAnimation.setDuration(2000);
        view.startAnimation(mLoadAnimation);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_who_is_it);

        if (!init) {
            // Enable Local Datastore.
            Parse.enableLocalDatastore(this);
            Parse.initialize(this, "DKdzMLGCHB2f0MyOy2Gl6p5dFgyCS7m9xFvlNYnU", "5NgNkrGLUlL4GXEeERjpqze0yl1p8zmsOEJ77WLk");
            init = true;
        }

        final Button parentSignUpButton = (Button) findViewById(R.id.parentSignUpBtn);
        final Button parentLoginButton = (Button) findViewById(R.id.parentLoginBtn);
        final Button childButton = (Button) findViewById(R.id.childBtn);

        // Parent Sign Up Button
        parentSignUpButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(whoIsItActivity.this, signupActivity.class);
                startActivity(intent);
            }
        });

        // Parent Login Button
        parentLoginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(whoIsItActivity.this, parentLoginActivity.class);
                startActivity(intent);
            }
        });

        // Child Button
        childButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(whoIsItActivity.this, childLoginActivity.class);
                startActivity(intent);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_who_is_it, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
