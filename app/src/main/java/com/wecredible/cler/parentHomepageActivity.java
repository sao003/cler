package com.wecredible.cler;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

/**
 * Created by pablo on 4/4/2015.
 */

public class parentHomepageActivity extends ActionBarActivity {
    DecimalFormat precision = new DecimalFormat("0.00");
    private String m_Text = "";
    private EditText input;
    private AlertDialog.Builder builder;

    private EditText currBalance;
    private EditText currIntRate;
    private EditText currLoaned;

    private String userID;
    private ParseQuery<ParseObject> query = ParseQuery.getQuery("target");

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_homepage);

        Parse.initialize(this, "DKdzMLGCHB2f0MyOy2Gl6p5dFgyCS7m9xFvlNYnU", "5NgNkrGLUlL4GXEeERjpqze0yl1p8zmsOEJ77WLk");

        currBalance = (EditText) findViewById(R.id.balance_view);
        currIntRate = (EditText) findViewById(R.id.interestRate_view);
        currLoaned = (EditText) findViewById(R.id.loaned_view);

        // disable user input
        currBalance.setKeyListener(null);
        currIntRate.setKeyListener(null);
        currLoaned.setKeyListener(null);

        query.getInBackground(CurrSession.getUserID(), new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    currBalance.setText((String) object.get("currentBalance").toString());
                    currencyFormatter(currBalance);
                    currIntRate.setText((String) object.get("interestRate").toString());
                    interestFormatter(currIntRate);
                    currLoaned.setText((String) object.get("loans").toString());
                    currencyFormatter(currLoaned);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

        final Button addBalanceButton = (Button) findViewById(R.id.add_balance_button);

        builder = new AlertDialog.Builder(this);


        addBalanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog("Add Balance");
            }
        });

        final Button transactionButton = (Button) findViewById(R.id.transactions_button);
        transactionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(parentHomepageActivity.this, childTransactionsActivity.class);
                startActivity(intent);
            }
        });

        final Button loansButton = (Button) findViewById(R.id.loans_button);
        loansButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(parentHomepageActivity.this, parentLoansActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_parent_homepage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDialog(String title) {
        builder.setTitle(title);



        // Set up the input
        input = new EditText(this);
        // Specify the type of input expected;
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        input.setRawInputType(Configuration.KEYBOARD_12KEY);
        input.setText("$");
        builder.setView(input);


        input.addTextChangedListener(new TextWatcher() {

            private String current = "";

            @Override
            public void afterTextChanged(Editable s) { }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    input.removeTextChangedListener(this);
                    String cleanString = s.toString().replaceAll("[^\\d]", "");
                    try {
                        double parsed = Double.parseDouble(cleanString);
                        String formatted = NumberFormat.getCurrencyInstance()
                                .format((parsed / 100));
                        current = formatted;
                        input.setText(formatted);
                        input.setSelection(formatted.length());
                        input.addTextChangedListener(this);
                    } catch (NumberFormatException e) {
                        // do nothing
                    }
                }
            }
        });

        // Set up the buttons
        builder.setPositiveButton("Add Balance", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
                if (!m_Text.equals("$"))
                    addBalance(m_Text);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();

    }

    private void currencyFormatter(EditText et) {
        String cleanString = et.getText().toString().replaceAll("[$,.]", "");
        double parsed = Double.parseDouble(cleanString);
        String s = NumberFormat.getCurrencyInstance()
                .format((parsed / 100));
        et.setText(s);
    }

    private void interestFormatter(EditText et) {
        String cleanString = et.getText().toString().replaceAll("[%,.]", "");
        double parsed = Double.parseDouble(cleanString);
        String s = NumberFormat.getNumberInstance()
                .format((parsed / 100));
        s += "%";
        et.setText(s);
    }

    private void addBalance(String added) {
        final ParseObject myObject = new ParseObject("target");

        String cleanAdded = added.replaceAll("[$,.]", "");
        String cleanCurrent = currBalance.getText().toString().replaceAll("[$,.]", "");
        final int parsedIntAdded = Integer.parseInt(cleanAdded);
        final int parsedIntCurrent = Integer.parseInt(cleanCurrent);

        // Retrieve the object by id
        query.getInBackground(CurrSession.getUserID(), new GetCallback<ParseObject>() {
            public void done(ParseObject user, ParseException e) {
                if (e == null) {
                    user.put("currentBalance", parsedIntAdded+parsedIntCurrent);
                    user.saveInBackground();

                    currBalance.setText((String) user.get("currentBalance").toString());
                    currencyFormatter(currBalance);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Logging Out")
            .setMessage("Are you sure you want to logout?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ParseUser.logOut();
                    CurrSession.setUserID(null);
                    CurrSession.setUsername(null);
                    finish();
                }

            })
            .setNegativeButton("No", null)
            .show();
    }
}
