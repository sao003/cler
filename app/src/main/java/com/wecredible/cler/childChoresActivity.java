package com.wecredible.cler;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 4/5/2015.
 */
public class childChoresActivity extends ActionBarActivity{

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_chores);

        final childChoresActivity me = this;

        ParseQuery<ParseObject> query = ParseQuery.getQuery("target");
        query.whereEqualTo("childCode", CurrSession.getUsername());
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> queryEntries, ParseException e) {
                if (e == null) {
                    String parent = queryEntries.get(0).getString("username");
                    System.out.println("parent: " + parent);
                    ParseQuery<ParseObject> query2 = ParseQuery.getQuery("Chores");
                    query2.whereEqualTo("user", parent);
                    query2.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> queryEntries, ParseException e) {
                            ArrayList<String> chores = new ArrayList<String>();
                            ArrayList<String> rewards = new ArrayList<String>();
                            if (e == null) {
                                for (ParseObject p : queryEntries) {
                                    chores.add(p.getString("Task"));
                                    int reward = p.getInt("Reward");
                                    if (reward % 100 < 10) {
                                        rewards.add("$" + p.getInt("Reward")/100 + ".0" + p.getInt("Reward")%100);
                                    } else {
                                        rewards.add("$" + p.getInt("Reward") / 100 + "." + p.getInt("Reward")%100);
                                    }
                                }
                                String[] chores_Arr, rewards_Arr;
                                if (chores.size() == 0) {
                                    chores_Arr = new String[]{};
                                    rewards_Arr = new String[]{};
                                } else {
                                    chores_Arr = chores.toArray(new String[chores.size()]);
                                    rewards_Arr = rewards.toArray(new String[rewards.size()]);
                                }
                                ListView listViewChore = (ListView) findViewById(R.id.task_listView);
                                ListViewAdapter lvAdapter = new ListViewAdapter(me, chores_Arr, rewards_Arr);
                                listViewChore.setAdapter(lvAdapter);

                            } else {
                                // something went wrong
                            }
                        }
                    });
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });


    }
}
