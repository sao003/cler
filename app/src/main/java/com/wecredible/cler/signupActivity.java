package com.wecredible.cler;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.Random;


public class signupActivity extends ActionBarActivity {
    private static String code;
    private CurrSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        final Button signupButton = (Button) findViewById(R.id.signUpBtn);

        final EditText username = (EditText) findViewById(R.id.sign_up_screen_username_edit_text);
        final EditText email = (EditText) findViewById(R.id.sign_up_screen_email_edit_text);
        final EditText childname = (EditText) findViewById(R.id.sign_up_screen_child_name_edit_text);
        final EditText password = (EditText) findViewById(R.id.sign_up_screen_password_edit_text);


        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser user = new ParseUser();
                user.setUsername(username.getText().toString());
                user.setEmail(email.getText().toString());
                user.setPassword(password.getText().toString());

                String userCode = genCode(username.getText().toString(), childname.getText().toString());
                ParseObject targetObject = new ParseObject("target");
                targetObject.put("childCode", userCode);
                targetObject.put("username", username.getText().toString());
                targetObject.put("childName", childname.getText().toString());
                targetObject.put("currentBalance", 0);
                targetObject.put("isInDebt", false);
                targetObject.put("interestRate", 0);
                targetObject.put("allowancePeriod", 0);
                targetObject.put("allowanceAmount", 0);
                targetObject.put("loans", 0);

                targetObject.saveInBackground();

                session = new CurrSession(username.getText().toString());

                user.signUpInBackground(new SignUpCallback() {
                    public void done(ParseException e) {
                        if (e == null) {
                            Intent intent = new Intent(signupActivity.this, parentCodeActivity.class);
                            startActivity(intent);
                        } else {
                        }
                    }
                });
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String genCode(String user, String child) {
        String result = "";

        result += child;
        result += user.substring(0,3);

        Random random = new Random();
        result += random.nextInt(20);

        setCode(result);

        return result;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static String getCode() {
        return code;
    }
}
