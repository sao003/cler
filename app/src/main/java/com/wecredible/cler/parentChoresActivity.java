package com.wecredible.cler;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.ParseObject;

import java.text.NumberFormat;

/**
 * Created by Andrew on 4/5/2015.
 */
public class parentChoresActivity extends ActionBarActivity{

    EditText task_ET;
    EditText reward_ET;
    Button submit_button;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_loans);

        task_ET = (EditText) findViewById(R.id.assignChores_view);
        reward_ET = (EditText) findViewById(R.id.reward_view_parent);
        submit_button = (Button) findViewById(R.id.chores_button);

        reward_ET.addTextChangedListener(new TextWatcher() {

            private String current = "";

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    reward_ET.removeTextChangedListener(this);
                    String cleanString = s.toString().replaceAll("[^\\d]", "");
                    try {
                        double parsed = Double.parseDouble(cleanString);
                        String formatted = NumberFormat.getCurrencyInstance()
                                .format((parsed / 100));
                        current = formatted;
                        reward_ET.setText(formatted);
                        reward_ET.setSelection(formatted.length());
                        reward_ET.addTextChangedListener(this);
                    } catch (NumberFormatException e) {
                        // do nothing
                    }
                }
            }
        });

        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String task = task_ET.getText().toString();
                String clearNumber = reward_ET.getText().toString().replaceAll("[^\\d]", "");
                int rewardAmnt = Integer.parseInt(clearNumber);
                ParseObject chore = new ParseObject("Chores");
                chore.put("Task", task);
                chore.put("Reward", rewardAmnt);
                chore.put("user", CurrSession.getUsername());
                chore.saveInBackground();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_parent_loans, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
