package com.wecredible.cleer;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;


public class childLoginActivity extends ActionBarActivity {

    private CurrSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_login);

        final TextView code = (TextView) findViewById(R.id.password_view);
        final TextView name = (TextView) findViewById(R.id.childName_view);
        final Button signInButton = (Button) findViewById(R.id.signIn_Button);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ParseQuery<ParseObject> query = ParseQuery.getQuery("target");
                query.whereEqualTo("childName", name.getText().toString()).whereEqualTo("childCode", code.getText().toString());
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> queryEntries, ParseException e) {
                        Log.d("size of list", queryEntries.size() + "");
                        if (e != null) {
                            Log.d("score", "Error: " + e.getMessage());
                        }
                        else if (queryEntries.size() > 0) { // eventually change this to while loop
                            session = new CurrSession(code.getText().toString());
                            Intent intent = new Intent(childLoginActivity.this, childHomepageActivity.class);

                            startActivity(intent);
                        } else {
                            Log.d("warning", "incorrect name or password");
                        }
                    }
                });
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_child_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
