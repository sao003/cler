package com.wecredible.cleer;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 4/4/2015.
 */
public class childLoansActivity extends ActionBarActivity {

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_loans);

        final EditText requestLoan_ET = (EditText) findViewById(R.id.requestLoan_view);
        final Button request_Button = (Button) findViewById(R.id.requestLoan_button);

        String[] s = new String[]{};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, s);

        ListView listView = (ListView) findViewById(R.id.chores_listView);
        listView.setAdapter(adapter);

        request_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ParseObject requestObject = new ParseObject("request");
                requestObject.put("amount", Integer.parseInt(requestLoan_ET.getText().toString()));
                requestObject.put("child", CurrSession.getUsername());
                ParseQuery<ParseObject> query = ParseQuery.getQuery("target");
                query.whereEqualTo("childCode", CurrSession.getUsername());
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> queryEntries, ParseException e) {
                        if (e == null) {
                            String parent = queryEntries.get(0).getString("username");
                            requestObject.put("parent", parent);
                        } else {
                            // something went wrong
                        }
                    }
                });

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_child_loans, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
