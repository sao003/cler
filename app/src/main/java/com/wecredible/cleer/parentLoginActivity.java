package com.wecredible.cleer;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;


public class parentLoginActivity extends ActionBarActivity {

    CurrSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_login);

        final EditText usernameET = (EditText) findViewById(R.id.login_username_edit_text);
        final EditText passwordET = (EditText) findViewById(R.id.login_password_edit_text);

        final Button loginButton = (Button) findViewById(R.id.loginConfirmBtn);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser.logInInBackground(usernameET.getText().toString().toLowerCase(), passwordET.getText().toString(), new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            session = new CurrSession(usernameET.getText().toString().toLowerCase());

                            ParseQuery<ParseObject> query = ParseQuery.getQuery("target");
                            query.whereEqualTo("username", usernameET.getText().toString().toLowerCase());
                            query.findInBackground(new FindCallback<ParseObject>() {
                               public void done(List<ParseObject> scoreList, ParseException e) {
                                   if (e == null) {
                                        session.setUserID((String) scoreList.get(0).getObjectId());
                                   }
                               }
                            });

                            Intent intent = new Intent(parentLoginActivity.this, parentCodeActivity.class);
                            startActivity(intent);
                        } else {
                            Log.d("score", "Error: " + e.getMessage());
                        }
                    }
                });
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_parent_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
