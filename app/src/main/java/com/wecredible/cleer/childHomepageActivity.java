package com.wecredible.cleer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

/**
 * Created by pablo on 4/4/2015.
 */

public class childHomepageActivity extends ActionBarActivity {
    private EditText currBalance;
    private EditText currIntRate;
    private EditText currLoaned;

    private ParseQuery<ParseObject> query = ParseQuery.getQuery("target");

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_homepage);

        currBalance = (EditText) findViewById(R.id.balance_view_child);
        currIntRate = (EditText) findViewById(R.id.interestRate_view_child);
        currLoaned = (EditText) findViewById(R.id.loans_view_child);

        // disable user input
        currBalance.setKeyListener(null);
        currIntRate.setKeyListener(null);
        currLoaned.setKeyListener(null);

        final Button loans_Button = (Button) findViewById(R.id.loans_button_child);
        final Button trans_Button = (Button) findViewById(R.id.transactions_button_child);

        query.whereEqualTo("childCode", CurrSession.getUsername());
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> queryEntries, ParseException e) {
                if (e == null) {
                    currBalance.setText((String) queryEntries.get(0).get("currentBalance").toString());
                    currencyFormatter(currBalance);
                    currIntRate.setText((String) queryEntries.get(0).get("interestRate").toString());
                    interestFormatter(currIntRate);
                    currLoaned.setText((String) queryEntries.get(0).get("loans").toString());
                    currencyFormatter(currLoaned);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

        loans_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(childHomepageActivity.this, childLoansActivity.class);
                startActivity(intent);
            }
        });

        trans_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(childHomepageActivity.this, childTransactionsActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_child_homepage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void currencyFormatter(EditText et) {
        String cleanString = et.getText().toString().replaceAll("[$,.]", "");
        double parsed = Double.parseDouble(cleanString);
        String s = NumberFormat.getCurrencyInstance()
                .format((parsed / 100));
        et.setText(s);
    }

    private void interestFormatter(EditText et) {
        String cleanString = et.getText().toString().replaceAll("[%,.]", "");
        double parsed = Double.parseDouble(cleanString);
        String s = NumberFormat.getNumberInstance()
                .format((parsed / 100));
        s += "%";
        et.setText(s);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Logging Out")
            .setMessage("Are you sure you want to logout?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    CurrSession.setUserID(null);
                    CurrSession.setUsername(null);
                    finish();
                }

            })
            .setNegativeButton("No", null)
            .show();
    }
}
