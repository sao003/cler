package com.wecredible.cleer;

/**
 * Created by Sean on 4/4/15.
 */
public class CurrSession {
    private static String username;
    private static String userID;

    public CurrSession(String username) {
        setUsername(username);
    }

    public static void setUsername(String username) {
        CurrSession.username = username;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUserID(String userID) { CurrSession.userID = userID; }

    public static String getUserID() {
        return userID;
    }
}
