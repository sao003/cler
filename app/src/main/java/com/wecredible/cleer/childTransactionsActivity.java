package com.wecredible.cleer;

/**
 * Created by Andrew on 4/4/2015.
 */

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;

public class childTransactionsActivity extends ActionBarActivity implements ActionBar.TabListener, android.support.v7.app.ActionBar.TabListener {

    android.support.v7.app.ActionBar actionbar;
    ViewPager viewpager;
    FragmentPageAdapter ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_transactions);
        viewpager = (ViewPager)findViewById(R.id.pager);
        ft = new FragmentPageAdapter(getSupportFragmentManager());

        actionbar = getSupportActionBar();
        viewpager.setAdapter(ft);
        actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionbar.addTab(actionbar.newTab().setText("All").setTabListener(this));
        actionbar.addTab(actionbar.newTab().setText("Deposits").setTabListener(this));
        actionbar.addTab(actionbar.newTab().setText("Loans").setTabListener(this));
    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
        //TODO Auto-generated method stub
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        //TODO Auto-generated method stub
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
        //TODO Auto-generated method stub
    }

    @Override
    public void onTabSelected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }
}
