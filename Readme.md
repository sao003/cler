#CLER: Children Learning Economics Revolutionized (UNDER DEVELOPMENT PROCESS)

##Problem
- Learning economics is one of the most important aspect, but many parents are not very sure when to teach their kids about the concept of "credit".
According to Tim Chen, the CEO of NerdWallet, and many other experts in economics, it is okay to teach the concept of credit when they are even in elementry school.

- But how?
The best way to teach is to teach how to manage money they have.


##Solution
###CLER is an app that can help building spending habits of children to teenagers.
####Features
- Keep track of transaction lists: Where did I spend? Where did I get money from?
- Instead of nagging to get more allowance, you children can now conceptually borrow money from you: Teach them how not to borrow money. However, it is also important to teach what to do once your children borrowed money.
- If your children cannot pay back, assign them jobs to clear debt!: Teach them to value money and labor.
- Give them star ratings about their spending habit: Work with your children to build better spending habits.


##Who should use this app?
1. If your children always need allowance.
2. If your children do not seem to value money.
3. If you want your children to know correct way to earn money.
4. If you have hard time monitoring what your children are spending their money on,



##Future updates
- More features
- Better design
- Simpler and easier communication with your children
- Easy version of vocabulary explanations for education purpose


##How to use
1. Parents
	- Create an account with child's first name (no spaces).
	- Let your child know the code that's given to you.
	- Add balance to give your child allowance. Be thoughtful.
	- Accept or deny loan request from your child. Try to explain the reason behind your decision using Message section. Your child will learn.
	- View how your child is using their money. Comment on their usage (under development).
	- Add simply star rating of your child's spending habit. Make money lender's decision based on the star rating (under development).
	
2. Child
	- Use your first name and Child code (ask your parents!) to login
	- Take a look at current status (your balance, interest rate, loan amount)
	- Request loan from parents if you are out of money. Remember, it's better to use what you have wisely.
	- Pay debt using your balance. If you cannot make payment at this moment, use payment option menu.
	- Select chorse that's assigned to you by parents to get your debt deducted. Labor makes money.
	- Use Transanction menu to keep your money flow record (under development). If you know where money goes, it's easier to save!
	- Review your star rating and comment to learn what to do with your allowance (under development).
